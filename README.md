Role Name
=========
This is the Consul-Server  role. This allow you stand up the servers using consul (open source)

Requirements
------------

VMS already setup
------------------
We need ensure that the servers can be reached via ansible user. If they are setup already.
The username for ansible is ansadm. 

The user will be setup on the gitlab runner machine and the ssh keys mounted at runtime via the gitlab runner.

Create VMS via Terraform to call to Ansible
-------------------------------------------

This will need to be worked in the CIE. You will use terraform to standup the VM. You will need to change some parameters such as:


  vsphere_datacenter                   = var.dc

  vsphere_cluster                     = var.ds_cluster

  vsphere_resource_pool               = var.vmrp

  vsphere_datastore                   = var.datastore

  vsphere_network                      = var.network

  vsphere_user                        = var.vsphere_user

  vsphere_password                    = var.vsphere_password

  vsphere_server                      = var.vsphere_server

Output information from Terraform
---------------------------------

all_consul_servers                    = gives the terraform output of the vms 
all_consul_servers_ips                = gives the default ip addresses of all vms. This will be used by ansible

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.



Heres is description of vars/main.yaml


Templates
----------
These are templates that can will be generated on the fly

server.hcl.j2
---

These are variables that need be set for the server.hcl.j2 template

server = true_or_false                               Value chosen:  true

Number of servers to build number_of_servers: 5      Value chosen: 5

Enabling the consul UI enable_ui: true               Value chosen: true

The bindaddr for head server bind_addr: '"bind_addr"' Value chosen: bind_addr_head_node

List of ip addresses in the cluster
array_ips: '["10.10.10.197", "10.10.10.198", "10.10.10.199"]'


The logging verbosity logging_verbosity: INFO          Value chosen: INFO
Value can be DEBUG


consul.service.j2
---
This file is the systemd service for start and stop of consul. 


consul.hcl.j2
---
This is where we place the configuration, encryption key, and datacenter name

datacenter name can be anything we have chose "cie"

data_dir is where you find information about consul service and data its collecting. Troubleshooting will be done here

encrypt is the encryption key each node will use to connect to the cluster


Dependencies
------------
there are no role dependencies in terms of other roles that need to be included

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

Placed in /etc/ansible/site.yaml
    ---
 - hosts: consul_servers
   roles:
   - consul-server


License
-------

BSD

Author Information
------------------
Tameika Reed
