# there should be an object store setup
# you can use minio or s3
# if airgapped use minio

terraform {
#  backend "local" {
#    path = "terraform.tfstate"
#  }
  backend "s3" {
    bucket = "consul-standup-tf-s3"
    key    = "aws/terraform.tfstate"
    region = "us-east-1"
  }
}

