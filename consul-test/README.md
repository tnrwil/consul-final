# Role Name 

This is the Consul-Test role allows you to test if the cluster is up or down.  

## Test 1
Check to see if the `local.keyring` is the same on all nodes in cluster.
 
 ## Test 2 
Run a the command `consul members` to ensure that the  `-bootstrap-expect` is met. 

This can be found in `server.hcl`  or `consul.hcl file`. 
>Depends on if you combined the files or not.

## Test 3
The `wget` command is performed on all nodes which is looking for return code `200`. If that does not occur then the cluster is not up. 

Next step would be to identify what is wrong by reading the error logs.








#### Author Information
------------------

_Tameika Reed_ 
