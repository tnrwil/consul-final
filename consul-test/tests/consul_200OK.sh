#!/bin/bash

echo "+++++++++++++++++++++++++++++++++++++++++++++++"
echo "ASSERT: Consul for status 200 OK"
echo "ASSERT: DOM contains Consul by HashiCorp"
echo "+++++++++++++++++++++++++++++++++++++++++++++++"

#Make a GET request and store it in a variable, the target URL/IP must be updated.
#var=$(wget --no-dns-cache https://demo.consul.io/ui/dc1/services -O -)
var=$(wget --no-dns-cache http://10.10.10.197:8500/ui -O -)

#Assert the html body contains the watchman title
if [[ $var == *"Consul by HashiCorp"* ]]; then
  echo "++++++++++++++++++++++++++++++++++++++++"
  echo "RESULT: DOM contains Consul by HashiCorp as expected"
  echo "++++++++++++++++++++++++++++++++++++++++"
fi
