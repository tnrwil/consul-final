## IPTables and consul_200Ok test ansible playbooks Ticket (962 and 977) 

### IPTables Instructions:
 - add and remove ports needed in the vars.yaml file. The iptables.yaml file will do the rest.

### Consul_200ok test Instructions:
 - You will have to change the "consul_test_url" in the vars.yaml file.  This url should point to the branch for your pipeline in gitlab-ce. This "post-test-consul.yaml file will pull down the consul_200ok.sh script to the "home_temp" variable in your vas.yaml file then execute the script.

#### Example vars.yaml
```sh
consul_test_url:  https://gitlab.cie.unclass.mil/djstocum/iptables-962/raw/master/Tests/src/test/resources
```
#### Edit the consul_200OK.sh file with address:8500/ui
 - Example:

 ```sh
 #!/bin/bash

 echo "+++++++++++++++++++++++++++++++++++++++++++++++"
 echo "ASSERT: Consul for status 200 OK"
 echo "ASSERT: DOM contains Consul by HashiCorp"
 echo "+++++++++++++++++++++++++++++++++++++++++++++++"

 #Make a GET request and store it in a variable, the target URL/IP must be updated.
 var=$(wget --no-dns-cache https://demo.consul.io/ui/dc1/services -O -)

 #Assert the html body contains the watchman title
 if [[ $var == *"Consul by HashiCorp"* ]]; then
   echo "++++++++++++++++++++++++++++++++++++++++"
     echo "RESULT: DOM contains Consul by HashiCorp as expected"
       echo "++++++++++++++++++++++++++++++++++++++++"
       fi
```


